﻿using ApiControllers.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiControllers.Controllers
{
    [Route("api/[controller]")]
    public class ReservationController : Controller
    {
        private readonly IRepository repository;

        public ReservationController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public IEnumerable<Reservation> Get()
        {
            return repository.Reservations;
        }

        [HttpGet("{id}")]
        public Reservation Get(int id)
        {
            return repository[id];
        }

        [HttpPost]
        public Reservation Post([FromBody] Reservation reservation)
        {
            return repository.AddReservation(new Reservation {
                ClientName = reservation.ClientName,
                Location = reservation.Location
            });
        }

        [HttpPut]
        public Reservation Put([FromBody] Reservation reservation)
        {
            return repository.UpdateReservation(reservation);
        }

        [HttpPatch("{id}")]
        public StatusCodeResult Patch(int id, [FromBody] JsonPatchDocument<Reservation> patch) 
        {
            Reservation res = repository[id];

            if(res != null)
            {
                patch.ApplyTo(res);
                return Ok();
            }
            return NotFound();
        }

        [HttpDelete("{id}")]
        public void Delete(int id) => repository.DeleteReservation(id);
    }
}
